package model;

public class Product {
	
	private int productID;
	private String productName;
	private String productMeasurement;
	private int productQuantity;
	private double productPrice;



public Product() {
	
}


public Product(String productName, int quantity, String productMeasurement, double productPrice) {
	
	this.setProductQuantity(quantity);
	this.setProductName(productName);
	this.setProductMeasurement(productMeasurement);
	this.setProductPrice(productPrice);

}


public int getProductID() {
	return productID;
}


public void setProductID(int productID) {
	this.productID = productID;
}


public String getProductName() {
	return productName;
}


public void setProductName(String productName) {
	this.productName = productName;
}


public String getProductMeasurement() {
	return productMeasurement;
}


public void setProductMeasurement(String productMeasurement) {
	this.productMeasurement = productMeasurement;
}


public double getProductPrice() {
	return productPrice;
}


public void setProductPrice(double productPrice) {
	this.productPrice = productPrice;
}

public String toString() {
	
	return this.productID + " " + this.productName + " " + this.productMeasurement + " " + this.productPrice + " " + this.productQuantity;
}


public int getProductQuantity() {
	return productQuantity;
}


public void setProductQuantity(int productQuantity) {
	this.productQuantity = productQuantity;
}



}