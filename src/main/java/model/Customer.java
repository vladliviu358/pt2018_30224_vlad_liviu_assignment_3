package model;

public class Customer {
	
	private int customerID;
	private String customerName;
	private String customerAddress;
	private String customerPhone;
	private String customerMail;
	private String customerStateAndCountry;
	
	public Customer() {
	}
	
	public Customer(String customerName, String customerAddress, String customerPhone, String customerMail, String customerStateAndCountry) {
		
		
		this.setCustomerName(customerName);
		this.setCustomerAddress(customerAddress);
		this.setCustomerPhone(customerPhone);
		this.setCustomerMail(customerMail);
		this.setCustomerStateAndCountry(customerStateAndCountry);
		
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerMail() {
		return customerMail;
	}

	public void setCustomerMail(String customerMail) {
		this.customerMail = customerMail;
	}

	public String getCustomerStateAndCountry() {
		return customerStateAndCountry;
	}

	public void setCustomerStateAndCountry(String customerStateAndCountry) {
		this.customerStateAndCountry = customerStateAndCountry;
	}
	
	public String toString() {
		
		return this.customerID + " " + this.customerName + " " + this.customerPhone + " " + this.customerAddress + " " + this.customerStateAndCountry + " " + this.customerMail;
	}
	

}
