package presentation;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import businessLayer.CustomerBLL;
import model.Customer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;

public class CustomerWindow {

	public JFrame frame;
	private JTextField searchTextField;
	private JTable table;
	private CustomerBLL customer;
	private JTextField phoneText;
	private JTextField nameText;
	private JTextField addressText;
	private JTextField mailText;
	private JTextField tarajudetText;

	public CustomerWindow() { 
		initialize();
		infoTable();
		initialInfo();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 0, 102));
		frame.setBounds(100, 100, 800, 575);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		frame.setTitle("Clienti");
		frame.setResizable(false);

		JButton addCustomer = new JButton("Adauga");
		addCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String name = nameText.getText();
				String address = addressText.getText();
				String phone = phoneText.getText();
				String mail = mailText.getText();
				String address1 = tarajudetText.getText();
				
				
				if(name != null && address != null && phone!=null && mail != null && address1!=null) {
					
					Customer addCustomer = new Customer(name,address,phone,mail,address1);
					
					try {
						customer = new CustomerBLL();
						if(customer.addCustomer(addCustomer) == true) {
						initialInfo();
						}
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}

			}
		});
		addCustomer.setFont(new Font("Times New Roman", Font.BOLD, 18));
		addCustomer.setBounds(470, 464, 150, 50);
		frame.getContentPane().add(addCustomer);

		JButton deleteCustomer = new JButton("Sterge");
		deleteCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int row = table.getSelectedRow();
				if(row < 0) {
					
					JOptionPane.showMessageDialog(null, "Selectati un client!", "Delete Warning", JOptionPane.INFORMATION_MESSAGE);
				}
				
				String selectedRow = table.getModel().getValueAt(row, 0).toString();
				int clientClick = Integer.parseInt(selectedRow);
				try {
					if(customer.deleteCustomer(clientClick) == true) {
						try {
							initialInfo();
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		deleteCustomer.setFont(new Font("Times New Roman", Font.BOLD, 18));
		deleteCustomer.setBounds(113, 464, 150, 50);
		frame.getContentPane().add(deleteCustomer);

		JButton searchClient = new JButton("Cauta");
		searchClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				ArrayList<Customer> customers = new ArrayList<Customer>();
				String searchName;
				searchName = searchTextField.getText();
				try {
					customer = new CustomerBLL();
				} catch (Exception eSearch) {
					eSearch.printStackTrace();
				}
				if (searchName != null && searchName.trim().length() > 0) {
					try {
						customers = customer.searchCustomer(searchName);
						System.out.println("found");
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}

				else
					try {
						customers = customer.getCustomers();
						System.out.println("found1");

					} catch (Exception e3) {
						e3.printStackTrace();
					}

				if (customers != null) {

					ClientTable clientModel = new ClientTable(customers);
					table.setModel(clientModel);

				}

			}
		});
		searchClient.setFont(new Font("Times New Roman", Font.BOLD, 18));
		searchClient.setBounds(511, 13, 109, 31);
		frame.getContentPane().add(searchClient);

		JButton changeClient = new JButton("Modifica");
		changeClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int row = table.getSelectedRow();
				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Alegeti un client", "Change Warning", JOptionPane.ERROR_MESSAGE);
				}
				
				String customerID = table.getModel().getValueAt(row, 0).toString();
				int ch = Integer.parseInt(customerID);

				
				String cName = nameText.getText();
				String cAddress = addressText.getText();
				String cPhone = phoneText.getText();
				String cMail = mailText.getText();
				String cAddress2 = tarajudetText.getText();
				
				Customer aux = new Customer();
				aux.setCustomerID(ch);
				aux.setCustomerName(cName);
				aux.setCustomerAddress(cAddress);
				aux.setCustomerPhone(cPhone);
				aux.setCustomerMail(cMail);
				aux.setCustomerStateAndCountry(cAddress2);
				
				if(customer.changeCustomer(aux) == true) {
					try{
						initialInfo();
					}
					catch (Exception e3) {
						e3.printStackTrace();
					}
				}
				
			}
		});
		changeClient.setFont(new Font("Times New Roman", Font.BOLD, 18));
		changeClient.setBounds(291, 464, 150, 50);
		frame.getContentPane().add(changeClient);

		searchTextField = new JTextField();
		searchTextField.setBounds(172, 16, 327, 26);
		frame.getContentPane().add(searchTextField);
		searchTextField.setColumns(10);

		JButton back = new JButton("Inapoi");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Warehouse wh = new Warehouse();
				wh.frame.setVisible(true);
				frame.dispose();
			}
		});
		back.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		back.setBounds(12, 17, 97, 25);
		frame.getContentPane().add(back);

	}

	private void infoTable() {

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 57, 758, 309);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		table.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		table.setRowHeight(40);
		table.setForeground(new Color(255, 255, 224));
		table.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), null, null, null));
		table.setBackground(Color.BLACK);
		scrollPane.setViewportView(table);
		
		phoneText = new JTextField();
		phoneText.setBounds(383, 379, 116, 22);
		frame.getContentPane().add(phoneText);
		phoneText.setColumns(10);
		
		nameText = new JTextField();
		nameText.setColumns(10);
		nameText.setBounds(113, 379, 116, 22);
		frame.getContentPane().add(nameText);
		
		addressText = new JTextField();
		addressText.setColumns(10);
		addressText.setBounds(113, 414, 116, 22);
		frame.getContentPane().add(addressText);
		
		mailText = new JTextField();
		mailText.setColumns(10);
		mailText.setBounds(383, 414, 116, 22);
		frame.getContentPane().add(mailText);
		
		JLabel lblNewLabel = new JLabel("Nume");
		lblNewLabel.setBounds(45, 382, 56, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblAdresa = new JLabel("Adresa");
		lblAdresa.setBounds(45, 417, 56, 16);
		frame.getContentPane().add(lblAdresa);
		
		JLabel lblTelefon = new JLabel("Telefon");
		lblTelefon.setBounds(315, 382, 56, 16);
		frame.getContentPane().add(lblTelefon);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setBounds(315, 417, 56, 16);
		frame.getContentPane().add(lblMail);
		
		tarajudetText = new JTextField();
		tarajudetText.setColumns(10);
		tarajudetText.setBounds(654, 379, 116, 22);
		frame.getContentPane().add(tarajudetText);
		
		JLabel lblTaraJudet = new JLabel("Tara Judet");
		lblTaraJudet.setBounds(567, 382, 75, 16);
		frame.getContentPane().add(lblTaraJudet);
		table.getTableHeader().setPreferredSize(new Dimension(35, 35));
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 16));
	}

	public void initialInfo() {

		ArrayList<Customer> customers = new ArrayList<Customer>();
		try {
			customer = new CustomerBLL();
		} catch (Exception eSearch) {
			eSearch.printStackTrace();
		}
		try {
			customers = customer.getCustomers();

		} catch (Exception e3) {
			e3.printStackTrace();
		}

		if (customers != null) {

			ClientTable clientModel = new ClientTable(customers);
			table.setModel(clientModel);
			

		}
	}
}
