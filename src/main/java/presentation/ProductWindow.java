package presentation;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import businessLayer.CustomerBLL;
import businessLayer.OrderBLL;
import businessLayer.ProductBLL;
import model.Customer;
import model.Order;
import model.Product;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;

public class ProductWindow {

	public JFrame frame;
	private JTextField searchTextField;
	private JTable table;
	private ProductBLL product;
	private JTextField quantityText;
	private JTextField nameText;
	private JTextField priceText;
	private JTextField umText;

	public ProductWindow() { 
		initialize();
		infoTable();
		initialInfo();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(60, 179, 113));
		frame.setBounds(100, 100, 800, 575);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		frame.setTitle("Produse");
		frame.setResizable(false);

		JButton addProduct = new JButton("Adauga");
		addProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String name = nameText.getText();
				String quantity = quantityText.getText();
				String um = umText.getText();
				String price = priceText.getText();
		
				
				
				if(name != null && quantity != null && um!=null && price != null) {
					
					double productPrice =  Double.parseDouble(price);
					int productQuantity = Integer.parseInt(quantity);
					Product addProduct = new Product(name,productQuantity,um,productPrice);
					
					try {
						product = new ProductBLL();
						if(product.addProduct(addProduct) == true) {
						initialInfo();
						}
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		addProduct.setFont(new Font("Times New Roman", Font.BOLD, 18));
		addProduct.setBounds(470, 464, 150, 50);
		frame.getContentPane().add(addProduct);

		JButton deleteProduct = new JButton("Sterge");
		deleteProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int row = table.getSelectedRow();
				if(row < 0) {
					
					JOptionPane.showMessageDialog(null, "Selectati un produs!", "Delete Warning", JOptionPane.INFORMATION_MESSAGE);
				}
				
				String selectedRow = table.getModel().getValueAt(row, 0).toString();
				int clientClick = Integer.parseInt(selectedRow);
				try {
					if(product.deleteProduct(clientClick) == true) {
						try {
							initialInfo();
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		deleteProduct.setFont(new Font("Times New Roman", Font.BOLD, 18));
		deleteProduct.setBounds(113, 464, 150, 50);
		frame.getContentPane().add(deleteProduct);

		JButton searchProduct = new JButton("Cauta");
		searchProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				ArrayList<Product> products = new ArrayList<Product>();
				String searchName;
				searchName = searchTextField.getText();
				try {
					product = new ProductBLL();
				} catch (Exception eSearch) {
					eSearch.printStackTrace();
				}
				if (searchName != null && searchName.trim().length() > 0) {
					try {
						products = product.searchProduct(searchName);
						System.out.println("found");
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}

				else
					try {
						products = product.getProduct();
						System.out.println("found1");

					} catch (Exception e3) {
						e3.printStackTrace();
					}

				if (products != null) {

					ProductTable clientModel = new ProductTable(products);
					table.setModel(clientModel);

				}

			}
		});
		searchProduct.setFont(new Font("Times New Roman", Font.BOLD, 18));
		searchProduct.setBounds(511, 13, 109, 31);
		frame.getContentPane().add(searchProduct);

		JButton changeProduct = new JButton("Modifica");
		changeProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int row = table.getSelectedRow();
				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Alegeti un produs", "Change Warning", JOptionPane.ERROR_MESSAGE);
				}
				
				String customerID = table.getModel().getValueAt(row, 0).toString();
				int ch = Integer.parseInt(customerID);

				
				String cName = nameText.getText();
				String cQuant = quantityText.getText();
				String cUM = umText.getText();
				String cPrice = priceText.getText();
				double price = Double.parseDouble(cPrice);
				int quant = Integer.parseInt(cQuant);
				
				Product aux = new Product();
				aux.setProductID(ch);
				aux.setProductName(cName);
				aux.setProductPrice(price);
				aux.setProductQuantity(quant);
				aux.setProductMeasurement(cUM);
				
				if(product.changeProduct(aux) == true) {
					try{
						initialInfo();
					}
					catch (Exception e3) {
						e3.printStackTrace();
					}
				}
				
			}
		});
		changeProduct.setFont(new Font("Times New Roman", Font.BOLD, 18));
		changeProduct.setBounds(291, 464, 150, 50);
		frame.getContentPane().add(changeProduct);

		searchTextField = new JTextField();
		searchTextField.setBounds(172, 16, 327, 26);
		frame.getContentPane().add(searchTextField);
		searchTextField.setColumns(10);

		JButton back = new JButton("Inapoi");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Warehouse wh = new Warehouse();
				wh.frame.setVisible(true);
				frame.dispose();
			}
		});
		back.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		back.setBounds(12, 17, 97, 25);
		frame.getContentPane().add(back);

	}

	private void infoTable() {

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 57, 758, 309);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		table.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		table.setRowHeight(40);
		table.setForeground(new Color(255, 255, 224));
		table.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), null, null, null));
		table.setBackground(Color.BLACK);
		scrollPane.setViewportView(table);
		
		umText = new JTextField();
		umText.setBounds(383, 379, 116, 22);
		frame.getContentPane().add(umText);
		umText.setColumns(10);
		
		nameText = new JTextField();
		nameText.setColumns(10);
		nameText.setBounds(113, 379, 116, 22);
		frame.getContentPane().add(nameText);
		
		quantityText = new JTextField();
		quantityText.setColumns(10);
		quantityText.setBounds(113, 414, 116, 22);
		frame.getContentPane().add(quantityText);
		
		priceText = new JTextField();
		priceText.setColumns(10);
		priceText.setBounds(383, 414, 116, 22);
		frame.getContentPane().add(priceText);
		
		JLabel lblNewLabel = new JLabel("Nume");
		lblNewLabel.setBounds(45, 382, 56, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblAdresa = new JLabel("Cantitate");
		lblAdresa.setBounds(45, 417, 56, 16);
		frame.getContentPane().add(lblAdresa);
		
		JLabel umText = new JLabel("UM");
		umText.setBounds(315, 382, 56, 16);
		frame.getContentPane().add(umText);
		
		JLabel priceText = new JLabel("Pret");
		priceText.setBounds(315, 417, 56, 16);
		frame.getContentPane().add(priceText);
		table.getTableHeader().setPreferredSize(new Dimension(35, 35));
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 16));
	}

	public void initialInfo() {

		ArrayList<Product> products = new ArrayList<Product>();
		try {
			product = new ProductBLL();
		} catch (Exception eSearch) {
			eSearch.printStackTrace();
		}
		try {
			products = product.getProduct();

		} catch (Exception e3) {
			e3.printStackTrace();
		}

		if (products != null) {

			ProductTable productModel = new ProductTable(products);
			table.setModel(productModel);

		}
	}
}
