package presentation;

import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import businessLayer.Reflexion;
import model.Customer;

public class ClientTable extends AbstractTableModel {	
	
	private String[] header = new String[6];
	private ArrayList<Customer> customers;
	private static final int customerID = 0;
	private static final int customerName = 1;
	private static final int customerAddress = 2;
	private static final int customerPhone = 3;
	private static final int customerMail = 4;
	private static final int customerAddress1 = 5;

	public ClientTable(ArrayList<Customer> customers) {

		this.customers = customers;
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		if (customers != null) {
			return customers.size();
		} else
			return 0;
	}

	public int getColumnCount() {
		// TODO Auto-generated method stub
		return header.length;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		
		Object[][] value = new Object[customers.size()][30];
		for(int j = 0; j<customers.size();j++) {	
			int i = 0;
		for (Field field : customers.get(0).getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				value[j][i] = field.get(customers.get(j));
				i++;
				
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
		
		switch (columnIndex) {
		case customerID:
			return value[rowIndex][0];
		case customerName:
			return value[rowIndex][1];
		case customerAddress:
			return value[rowIndex][2];
		case customerPhone:
			return value[rowIndex][3];
		case customerMail:
			return value[rowIndex][4];
		case customerAddress1:
			return value[rowIndex][5];
		default:
			return value[rowIndex][0];
		}
	}

	public String getColumnName(int column) {
		
		int j = 0;
		for (Field field : customers.get(0).getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				header[j] = field.getName();
				j++;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		return header[column];
		
	}

}
