package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import model.Order;
import model.Product;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import businessLayer.OrderBLL;
import businessLayer.ProductBLL;
import businessLayer.Reflexion;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class OrderWindow {

	public JFrame frame;
	private JTextField searchTextField;
	private JTable table;
	private JTextField idClient;
	private JTextField idProdus;
	private JTextField cantitate;
	private OrderBLL order;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// CustomerWindow window = new CustomerWindow();
	// window.frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the application.
	 */
	public OrderWindow() {
		initialize();
		infoTable();
		initialInfo();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(51, 204, 153));
		frame.setBounds(100, 100, 800, 575);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		frame.setTitle("Comenzi");
		frame.setResizable(false);

		JButton sendOrder = new JButton("Facturare");
		sendOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int row = table.getSelectedRow();
				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Selectati o comanda!", "Receip Warning",
							JOptionPane.INFORMATION_MESSAGE);
				}
				try {
					String selectedRow = table.getModel().getValueAt(row, 0).toString();
					Document doc = new Document();
					PdfWriter.getInstance(doc,
							new FileOutputStream("Factura " + order.searchOrderF(selectedRow).getOrderID() + " "
									+ order.searchOrderF(selectedRow).getOrderDate() + ".pdf"));
					doc.open();
					doc.addTitle("Factura");
					String prop;
					prop = String.valueOf(Reflexion.retrieveProperties(order.searchOrderF(selectedRow)));
					doc.add(new Paragraph("Date Factura"));
					doc.add(new Paragraph("ID" + " " + "ClientID" + " " + "ProdusID" + " " + "Cantitate" + " " + "Pret"
							+ " " + "Data"));
					doc.add(new Paragraph(prop));
					doc.close();
					JOptionPane.showMessageDialog(null, "Comanda a fost tiparita!", "Order info",
							JOptionPane.INFORMATION_MESSAGE);

				} catch (Exception e5) {
					e5.printStackTrace();
				}

			}
		});
		sendOrder.setFont(new Font("Times New Roman", Font.BOLD, 18));
		sendOrder.setBounds(279, 464, 150, 50);
		frame.getContentPane().add(sendOrder);

		JButton deleteOrder = new JButton("Sterge");
		deleteOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int row = table.getSelectedRow();
				if (row < 0) {
					JOptionPane.showMessageDialog(null, "Selectati o comanda!", "Delete Warning",
							JOptionPane.INFORMATION_MESSAGE);
				}
				try {
					String selectedRow = table.getModel().getValueAt(row, 0).toString();
					int orderID = Integer.parseInt(selectedRow);
					if (order.deleteOrder(orderID) == true) {

						initialInfo();
					}

				} catch (Exception e5) {
					e5.printStackTrace();
				}
			}
		});
		deleteOrder.setFont(new Font("Times New Roman", Font.BOLD, 18));
		deleteOrder.setBounds(98, 464, 150, 50);
		frame.getContentPane().add(deleteOrder);

		JButton searchOrder = new JButton("Cauta");
		searchOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				ArrayList<Order> orders = new ArrayList<Order>();
				String searchOrder;
				searchOrder = searchTextField.getText();
				try {
					order = new OrderBLL();
				} catch (Exception e3) {
					e3.printStackTrace();
				}

				if (searchOrder != null && searchOrder.trim().length() > 0) {
					try {
						orders = order.searchOrder(searchOrder);
					} catch (Exception e01) {
						e01.printStackTrace();
					}
				}

				else
					try {
						orders = order.getOrder();
					} catch (Exception os) {
						os.printStackTrace();
					}

				if (orders != null) {
					OrderTable orderModel = new OrderTable(orders);
					table.setModel(orderModel);
				}
			}
		});
		searchOrder.setFont(new Font("Times New Roman", Font.BOLD, 18));
		searchOrder.setBounds(511, 13, 109, 31);
		frame.getContentPane().add(searchOrder);

		searchTextField = new JTextField();
		searchTextField.setBounds(172, 16, 327, 26);
		frame.getContentPane().add(searchTextField);
		searchTextField.setColumns(10);

		JButton back = new JButton("Inapoi");
		back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Warehouse wh = new Warehouse();
				wh.frame.setVisible(true);
				frame.dispose();
			}
		});
		back.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		back.setBounds(12, 17, 97, 25);
		frame.getContentPane().add(back);

	}

	private void infoTable() {

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 57, 758, 291);
		frame.getContentPane().add(scrollPane);

		table = new JTable();
		table.setFont(new Font("Trebuchet MS", Font.PLAIN, 16));
		table.setRowHeight(40);
		table.setForeground(new Color(255, 255, 224));
		table.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), null, null, null));
		table.setBackground(Color.BLACK);
		scrollPane.setViewportView(table);

		JButton addOrder = new JButton("Adauga");
		addOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String customerID = idClient.getText();
				String productID = idProdus.getText();
				String quantity = cantitate.getText();
				Date date = new Date();
				ProductBLL productBLL;

				if (customerID != null && productID != null && quantity != null) {

					int orderQuantity = Integer.parseInt(quantity);
					int customer = Integer.parseInt(customerID);
					int product = Integer.parseInt(productID);
					Order addOrder = new Order();
					addOrder.setCustomerID(customer);
					addOrder.setProductID(product);
					addOrder.setQuantity(orderQuantity);
					addOrder.setOrderDate(date);
					try {
						productBLL = new ProductBLL();
						if ((double) orderQuantity <= productBLL.searchProductID(productID).getProductQuantity()) {

							addOrder.setOrderPrice(
									(double) orderQuantity * productBLL.searchProductID(productID).getProductPrice());
							order = new OrderBLL();
							order.addOrder(addOrder);
							initialInfo();
							int q;
							q = productBLL.searchProductID(productID).getProductQuantity() - orderQuantity;
							productBLL.updateProduct(q);
						} else {
							JOptionPane.showMessageDialog(null, "Stoc insuficient!", "Stoc info",
									JOptionPane.INFORMATION_MESSAGE);
						}

					} catch (ClassNotFoundException e) {

						e.printStackTrace();
					} catch (SQLException e) {

						e.printStackTrace();
					} catch (Exception eADD) {
						eADD.printStackTrace();
					}
				}

			}
		});
		addOrder.setFont(new Font("Times New Roman", Font.BOLD, 18));
		addOrder.setBounds(459, 464, 150, 50);
		frame.getContentPane().add(addOrder);

		JLabel lblNewLabel = new JLabel("ID Client");
		lblNewLabel.setBounds(126, 361, 67, 31);
		frame.getContentPane().add(lblNewLabel);

		idClient = new JTextField();
		idClient.setBounds(203, 365, 116, 22);
		frame.getContentPane().add(idClient);
		idClient.setColumns(10);

		JLabel lblIdProdus = new JLabel("ID Produs");
		lblIdProdus.setBounds(126, 405, 67, 31);
		frame.getContentPane().add(lblIdProdus);

		JLabel lblCantitate = new JLabel("Cantitate");
		lblCantitate.setBounds(388, 361, 67, 31);
		frame.getContentPane().add(lblCantitate);

		idProdus = new JTextField();
		idProdus.setColumns(10);
		idProdus.setBounds(203, 409, 116, 22);
		frame.getContentPane().add(idProdus);

		cantitate = new JTextField();
		cantitate.setColumns(10);
		cantitate.setBounds(459, 365, 116, 22);
		frame.getContentPane().add(cantitate);
		table.getTableHeader().setPreferredSize(new Dimension(35, 35));
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 16));

	}

	public void initialInfo() {

		ArrayList<Order> orders = new ArrayList<Order>();

		try {
			order = new OrderBLL();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			orders = order.getOrder();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (orders != null) {

			 OrderTable orderModel = new OrderTable(orders);
			 table.setModel(orderModel);
			 
		}
				
	}
}
