package presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.SystemColor;
 
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Warehouse {

	public JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Warehouse window = new Warehouse();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Warehouse() {
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.info);
		frame.getContentPane().setForeground(new Color(255, 239, 213));
		frame.setBounds(100, 100, 800, 575);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Warehouse Manager");
        frame.setResizable(false);
		JButton client = new JButton("CLIENTI");
		client.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				CustomerWindow cw = new CustomerWindow();
				cw.frame.setVisible(true);
				frame.dispose();
				
			}
		});
		client.setBackground(Color.LIGHT_GRAY);
		client.setFont(new Font("Times New Roman", Font.BOLD, 18));
		client.setBounds(524, 285, 150, 50);
		frame.getContentPane().add(client);
		
		JButton product = new JButton("PRODUSE");
		product.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ProductWindow pw = new ProductWindow();
				pw.frame.setVisible(true);
				frame.dispose();
			}
		});
		product.setBackground(Color.LIGHT_GRAY);
		product.setForeground(Color.BLACK);
		product.setFont(new Font("Times New Roman", Font.BOLD, 18));
		product.setBounds(289, 285, 150, 50);
		frame.getContentPane().add(product);
		
		JButton order = new JButton("COMENZI");
		order.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				OrderWindow ow = new OrderWindow();
				ow.frame.setVisible(true);
				frame.dispose();
			}
		});
		order.setBackground(Color.LIGHT_GRAY);
		order.setFont(new Font("Times New Roman", Font.BOLD, 18));
		order.setBounds(43, 285, 150, 50);
		frame.getContentPane().add(order);
		
		JLabel label1 = new JLabel("Warehouse Manager");
		label1.setForeground(Color.DARK_GRAY);
		label1.setFont(new Font("Times New Roman", Font.PLAIN, 30));
		label1.setBounds(247, 81, 320, 155);
		frame.getContentPane().add(label1);
	}
}
