package presentation;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.mysql.jdbc.Field;
import model.Product;

public class ProductTable extends AbstractTableModel {

	//private String[] columns = { "ID", "Nume", "Cantitate", "UnitMasura", "Pret" };
	private String[] header = new String[5];
	private ArrayList<Product> products;
	private static final int productID = 0;
	private static final int productName = 1;
	private static final int productQuantity = 2;
	private static final int productMeasure = 3;
	private static final int productPrice = 4;

	public ProductTable(ArrayList<Product> products) {

		this.products = products;
	}

	public int getColumnCount() {
		// TODO Auto-generated method stub
		return header.length;
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		if (products != null) {
			return products.size();
		} else
			return 0;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		
		Object[][] value = new Object[products.size()][30];
		for(int j = 0; j<products.size();j++) {	
			int i = 0;
		for (java.lang.reflect.Field field : products.get(0).getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				value[j][i] = field.get(products.get(j));
				i++;
				
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
		switch (columnIndex) {
		case productID:
			return value[rowIndex][0];
		case productName:
			return value[rowIndex][1];
		case productQuantity:
			return value[rowIndex][2];
		case productMeasure:
			return value[rowIndex][3];
		case productPrice:
			return value[rowIndex][4];
		default:
			return value[rowIndex][0];
		}
	}

	public String getColumnName(int column) {
		int j = 0;
		for (java.lang.reflect.Field field : products.get(0).getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				header[j] = field.getName();
				j++;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		return header[column];
	}

}
