package presentation;

import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import businessLayer.Reflexion;
import model.Order;

public class OrderTable extends AbstractTableModel {

	private ArrayList<Order> orders;
	private String[] header = new String[6];
	private static final int orderID = 0;
	private static final int customerID = 1;
	private static final int productID = 2;
	private static final int quantity = 3;
	private static final int price = 4;
	private static final int date = 5;

//	public OrderTable(ArrayList<Order> orders) {
//
//		this.orders = orders;
//	
//	}
	
	public OrderTable(ArrayList<Order> orders) {

		this.orders = orders;
		
	}
	

	public int getRowCount() {
		// TODO Auto-generated method stub
		if (orders != null) {
			return orders.size();
		} else
			return 0;
	}

	public int getColumnCount() {
		// TODO Auto-generated method stub
		return header.length;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		 
		Object[][] value = new Object[orders.size()][30];
			for(int j = 0; j<orders.size();j++) {	
				int i = 0;
			for (Field field : orders.get(0).getClass().getDeclaredFields()) {
				field.setAccessible(true);
				try {
					value[j][i] = field.get(orders.get(j));
					i++;
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		switch (columnIndex) {
		case orderID:
			return value[rowIndex][0];
		case customerID:
			return value[rowIndex][1];
		case productID:
			return value[rowIndex][2];
		case quantity:
			return value[rowIndex][3];
		case price:
			return value[rowIndex][4];
		case date:
			return value[rowIndex][5];
		default:
			return value[rowIndex][0];
		}
		
	}

	public String getColumnName(int column) {
		int j = 0;
		for (Field field : orders.get(0).getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				header[j] = field.getName();
				j++;
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		return header[column];
	}

}
