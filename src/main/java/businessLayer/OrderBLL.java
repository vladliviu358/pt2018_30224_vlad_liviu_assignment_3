package businessLayer;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import dataAccessLayer.OrderDAO;
import model.Order;

public class OrderBLL {
	
	private OrderDAO orderDAO;
	
	public OrderBLL() throws Exception{
		
		orderDAO = new OrderDAO();
	}
	
	public ArrayList<Order> getOrder() throws SQLException {

//		if (orderDAO.getOrders().isEmpty()) {
//
//			JOptionPane.showMessageDialog(null, "Nu exista comenzi!", "Error Message", JOptionPane.INFORMATION_MESSAGE);
//			return null;
//
//		}

		return orderDAO.getOrders();
	}
	
	public boolean addOrder(Order order) {

		boolean checkAdd = false;

		try {
			checkAdd = orderDAO.addOrder(order);
			JOptionPane.showMessageDialog(null, "Comanda adaugata cu succes!", "ADD Info",
					JOptionPane.INFORMATION_MESSAGE);
			checkAdd = true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Comanda nu a putut fi adaugata", "ADD Error",
					JOptionPane.ERROR_MESSAGE);
			checkAdd = false;
		}
		return checkAdd;
	}
	
	public boolean deleteOrder(int orderID) {

		boolean checkDelete = false;

		try {
			if (orderDAO.deleteOrder(orderID) == true) {

				checkDelete = true;
				JOptionPane.showMessageDialog(null, "Comanda stersa cu succes!", "Delete Info",
						JOptionPane.INFORMATION_MESSAGE);

			} 
			else {
				
				checkDelete = false;
				JOptionPane.showMessageDialog(null, "Comanda nu a putut fi stersa", "Delete Error", JOptionPane.ERROR_MESSAGE);

			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		
		return checkDelete;
	}
	
	public ArrayList<Order> searchOrder(String id) throws SQLException {
		
		return orderDAO.searchOrder(id);
	}
	
	public Order searchOrderF(String id) throws SQLException {
		
		return orderDAO.searchOrderF(id);
	}
}
