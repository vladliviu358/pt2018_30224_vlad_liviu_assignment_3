package businessLayer;

import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.JTable;

public class Reflexion {
	
	
	public static String retrieveProperties(Object object) {
		
		Object value = null;
		String data = "";
		for (Field field : object.getClass().getDeclaredFields()) {
			
			field.setAccessible(true); // set modifier to public
			try {
				value = field.get(object);
				data += value.toString();
				data += "     ";
				System.out.println(data);
			//	System.out.println(field.getName() + " = " + value);
			System.out.println(field.getName());
				
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return data;
	}
	
	public static Object retrieveTableProperties(Object object) {
		
		Object value = null;
		for (Field field:object.getClass().getDeclaredFields()) {
			
			field.setAccessible(true);
			try {
			
				value = field.get(object);
				//System.out.println(value);
				
			}
		 catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		}
		return value;
	}
	
	}


