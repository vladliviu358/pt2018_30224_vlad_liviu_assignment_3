package businessLayer;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import dataAccessLayer.ProductDAO;
import model.Product;

public class ProductBLL {

	private ProductDAO productDAO;

	public ProductBLL() throws ClassNotFoundException, SQLException {

		productDAO = new ProductDAO();

	}

	public ArrayList<Product> getProduct() throws SQLException {

//		if (productDAO.getProducts().isEmpty()) {
//
//			JOptionPane.showMessageDialog(null, "Nu exista produse!", "Error Message", JOptionPane.INFORMATION_MESSAGE);
//			return null;
//
//		}

		return productDAO.getProducts();
	}

	public boolean addProduct(Product product) {

		boolean checkAdd = false;

		try {
			checkAdd = productDAO.addProduct(product);
			JOptionPane.showMessageDialog(null, "Produs adaugat cu succes!", "ADD Info",
					JOptionPane.INFORMATION_MESSAGE);
			checkAdd = true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Produsul nu a putut fi adaugat", "ADD Error",
					JOptionPane.ERROR_MESSAGE);
			checkAdd = false;
		}
		return checkAdd;
	}

	public boolean changeProduct(Product product) {

		boolean checkChange = false;

		try {
			checkChange = productDAO.changeProduct(product);
			JOptionPane.showMessageDialog(null, "Produs modificat cu succes!", "Change Info",
					JOptionPane.INFORMATION_MESSAGE);
			checkChange = true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Produsul nu a putut fi modificat", "Change Error",
					JOptionPane.ERROR_MESSAGE);
			checkChange = false;
		}

		return checkChange;
	}
	
	public boolean updateProduct(int quantity) {
		boolean checkChange = false;

		try {
			checkChange = productDAO.updateProduct(quantity);
//			JOptionPane.showMessageDialog(null, "Produs modificat cu succes!", "Change Info",
//					JOptionPane.INFORMATION_MESSAGE);
			checkChange = true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Produsul nu a putut fi modificat", "Change Error",
					JOptionPane.ERROR_MESSAGE);
			checkChange = false;
		}

		return checkChange;
	}

	public boolean deleteProduct(int productID) {

		boolean checkDelete = false;

		try {
			if (productDAO.deleteProduct(productID) == true) {

				checkDelete = true;
				JOptionPane.showMessageDialog(null, "Produs sters cu succes!", "Delete Info",
						JOptionPane.INFORMATION_MESSAGE);

			} 
			else {
				
				checkDelete = false;
				JOptionPane.showMessageDialog(null, "Produsul nu a putut fi sters", "Delete Error", JOptionPane.ERROR_MESSAGE);

			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		
		return checkDelete;
	}
	
	public ArrayList<Product> searchProduct(String name) throws SQLException{
		
//		try {
//			if(productDAO.searchProduct(name).size() == 0) {
//				JOptionPane.showMessageDialog(null, "Produsul nu a putut fi gasit", "Search Error", JOptionPane.ERROR_MESSAGE);
//				return null;
//			}
//		}
//		catch(Exception e) {
//			e.printStackTrace();
//		}
		return productDAO.searchProduct(name);
	}
	
public Product searchProductID(String name) throws SQLException{
		
//		try {
//			if(productDAO.searchProduct(name).size() == 0) {
//				JOptionPane.showMessageDialog(null, "Produsul nu a putut fi gasit", "Search Error", JOptionPane.ERROR_MESSAGE);
//				return null;
//			}
//		}
//		catch(Exception e) {
//			e.printStackTrace();
//		}
		return productDAO.searchProductID(name);
	}
}
