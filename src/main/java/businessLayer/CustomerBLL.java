package businessLayer;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import dataAccessLayer.CustomerDAO;
import model.Customer;

public class CustomerBLL {

	private CustomerDAO customerDAO;

	public CustomerBLL() throws ClassNotFoundException, SQLException {

		customerDAO = new CustomerDAO();
		
	}

	public ArrayList<Customer> getCustomers() throws SQLException {
		
//		if (customerDAO.getCustomers().isEmpty()) {
//
//			JOptionPane.showMessageDialog(null, "Nu exista clienti!", "Error Message", JOptionPane.INFORMATION_MESSAGE);
//			return null;
//		}
		
		return customerDAO.getCustomers();
		
	}

	public boolean addCustomer(Customer customer) {

		boolean checkAdd = false;

		try {
			checkAdd = customerDAO.addCustomer(customer);
			JOptionPane.showMessageDialog(null, "Client adaugat cu succes!", "ADD Info",
					JOptionPane.INFORMATION_MESSAGE);
			checkAdd = true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Clientul nu a putut fi adaugat", "ADD Error",
					JOptionPane.ERROR_MESSAGE);
			checkAdd = false;
		}
		return checkAdd;
	}

	public boolean changeCustomer(Customer customer) {

		boolean checkChange = false;

		try {
			checkChange = customerDAO.changeCustomer(customer);
			JOptionPane.showMessageDialog(null, "Clientul modificat cu succes!", "Change Info",
					JOptionPane.INFORMATION_MESSAGE);
			checkChange = true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Clientul nu a putut fi modificat", "Change Error",
					JOptionPane.ERROR_MESSAGE);
			checkChange = false;
		}

		return checkChange;
	}

	public boolean deleteCustomer(int customerID) {

		boolean checkDelete = false;

		try {
			if (customerDAO.deleteCustomer(customerID) == true) {

				checkDelete = true;
				JOptionPane.showMessageDialog(null, "Client sters cu succes!", "Delete Info",
						JOptionPane.INFORMATION_MESSAGE);

			} 
			else {
				
				checkDelete = false;
				JOptionPane.showMessageDialog(null, "Clientul nu a putut fi sters", "Delete Error", JOptionPane.ERROR_MESSAGE);

			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		
		return checkDelete;
	}
	
	public ArrayList<Customer> searchCustomer(String name) throws SQLException{
		
//		try {
//			if(customerDAO.searchCustomer(name).size() == 0) {
//				JOptionPane.showMessageDialog(null, "Clientul nu a putut fi gasit", "Search Error", JOptionPane.ERROR_MESSAGE);
//				return null;
//			}
//		}
//		catch(Exception e) {
//			e.printStackTrace();
//		}
		return customerDAO.searchCustomer(name);
	}

}
