package dataAccessLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnection {
	
	public static Connection connection;
	
	public static Connection connect() throws ClassNotFoundException, SQLException {
		
		String url = "jdbc:mysql://localhost:3306/warehousedb";
		String user = "root";
		String driver = "com.mysql.jdbc.Driver";
		String password = null;
		System.out.println("Connecting . . . ");
		try {
			
			Class.forName(driver);
			connection = (Connection) DriverManager.getConnection(url, user, password);
			System.out.println("Connected successfully!");
		}
		catch(Exception e) {
			
			System.out.println("Error on DataBaseConnection...");
			e.printStackTrace();
		}
		
		return connection;
	}
	
	public static Connection close() {
		try {
			connection.close();
			System.out.println("Connection with database terminated!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return connection;
	}

}
