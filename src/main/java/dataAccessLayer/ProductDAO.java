package dataAccessLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import model.Product;

public class ProductDAO {

	private Connection connection;
	private Product product;
	private Statement st = null;
	private ArrayList<Product> products = new ArrayList<Product>();
	private PreparedStatement statement = null;
	private ResultSet rs = null;

	public ProductDAO() throws ClassNotFoundException, SQLException {
		connection = DataBaseConnection.connect();
	}
	
	public ArrayList<Product> getProducts() throws SQLException {

		try {
			st = (Statement) connection.createStatement();
			rs = st.executeQuery("Select * from produs");

			while (rs.next()) {

				product = new Product();
				product.setProductID(rs.getInt(1));
				product.setProductName(rs.getString(2));
				product.setProductQuantity(rs.getInt(3));
				product.setProductMeasurement(rs.getString(4));
				product.setProductPrice(rs.getDouble(5));
				products.add(product);
			}

			return products;
		} catch (Exception e) {
			System.out.println("Error on getProducts!");
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<Product> searchProduct(String productName) throws SQLException {

		try {

			productName += "%";
			statement = (PreparedStatement) connection.prepareStatement("select * from produs where nume like ?");
			statement.setString(1, productName);
			rs = statement.executeQuery();

			while (rs.next()) {

				product = new Product();
				product.setProductID(rs.getInt(1));
				product.setProductName(rs.getString(2));
				product.setProductQuantity(rs.getInt(3));
				product.setProductMeasurement(rs.getString(4));
				product.setProductPrice(rs.getDouble(5));
				products.add(product);
			}
			return products;
		} catch (Exception e) {
			System.out.println("Error on searchProduct!");
			e.printStackTrace();
			return null;
		}
	}
	
	public Product searchProductID(String productName) throws SQLException {

		try {

			productName += "%";
			statement = (PreparedStatement) connection.prepareStatement("select * from produs where idProdus like ?");
			statement.setString(1, productName);
			rs = statement.executeQuery();

			while (rs.next()) {

				product = new Product();
				product.setProductID(rs.getInt(1));
				product.setProductName(rs.getString(2));
				product.setProductQuantity(rs.getInt(3));
				product.setProductMeasurement(rs.getString(4));
				product.setProductPrice(rs.getDouble(5));
				products.add(product);
			}
			return product;
		} catch (Exception e) {
			System.out.println("Error on searchProductID!");
			e.printStackTrace();
			return null;
		}
	}

	public boolean addProduct(Product product) throws SQLException {
		boolean checkAdd = false;
		try {
			statement = (PreparedStatement) connection.prepareStatement("insert into produs" + " (nume,cantitate,unitmasura,pret)" + " values( ?,?,?,?)");

			statement.setString(1, product.getProductName());
			statement.setInt(2, product.getProductQuantity());
			statement.setString(3, product.getProductMeasurement());
			statement.setDouble(4, product.getProductPrice());
			statement.executeUpdate();
			checkAdd = true;
		} catch (Exception e) {
			System.out.println("Error on addProduct!");
			e.printStackTrace();
			checkAdd = false;
		} 
		return checkAdd;
	}
	
	public boolean changeProduct(Product product) throws SQLException{
		
		boolean checkChange = false;
		
		try {
			
			statement = (PreparedStatement) connection.prepareStatement("update produs" + " set nume=?,cantitate=?,unitmasura=?,pret=?" + " where idProdus=?");
			
			statement.setString(1, product.getProductName());
			statement.setInt(2, product.getProductQuantity());
			statement.setString(3, product.getProductMeasurement());
			statement.setDouble(4, product.getProductPrice());
			statement.setInt(5, product.getProductID());
			statement.executeUpdate();
			checkChange = true;
		}
		catch (Exception e) {
			System.out.println("Error on changeProduct");
			e.printStackTrace();
			checkChange = false;
		}
		
		return checkChange;
	}
	
public boolean updateProduct(int quantity) throws SQLException{
		
		boolean checkChange = false;
		
		try {
			
			statement = (PreparedStatement) connection.prepareStatement("update produs" + " set nume=?,cantitate=?,unitmasura=?,pret=?" + " where idProdus=?");
			
			statement.setString(1, product.getProductName());
			statement.setInt(2,quantity);
			statement.setString(3, product.getProductMeasurement());
			statement.setDouble(4, product.getProductPrice());
			statement.setInt(5, product.getProductID());
			statement.executeUpdate();
			checkChange = true;
		}
		catch (Exception e) {
			System.out.println("Error on changeProduct");
			e.printStackTrace();
			checkChange = false;
		}
		
		return checkChange;
	}
	
	public boolean deleteProduct (int productID) throws SQLException{
		
		boolean checkDelete = false;
		
		try {
			
			
			statement = (PreparedStatement) connection.prepareStatement("delete from produs where idProdus=?");
			statement.setInt(1, productID);
			statement.executeUpdate();
			checkDelete = true;
		}
		
		catch (Exception e) {
			
			System.out.println("Error on deleteProduct");
			e.printStackTrace();
			checkDelete = false;
		}
		
		return checkDelete;
	}
	

}
