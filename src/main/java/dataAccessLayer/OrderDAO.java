package dataAccessLayer;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import model.Customer;
import model.Order;

public class OrderDAO {
	
	private Connection connection;
	private Order order;
	private PreparedStatement statement = null;
	private Statement st = null;
	private ResultSet rs = null;
	private ArrayList<Order> orders = new ArrayList<Order>();
	
	public OrderDAO() throws ClassNotFoundException, SQLException{
		
		connection = DataBaseConnection.connect();
	}
	
	
	public ArrayList<Order> getOrders() throws SQLException{
		
		try {
			
			st = (Statement) connection.createStatement();
			rs = st.executeQuery("select * from comanda");
			
			while(rs.next()) {
				order = new Order();
				order.setOrderID(rs.getInt(1));
				order.setCustomerID(rs.getInt(2));
				order.setProductID(rs.getInt(3));
				order.setQuantity(rs.getInt(4));
				order.setOrderPrice(rs.getDouble(5));
				Date currentDate = rs.getDate(6);
				order.setOrderDate(currentDate);
				orders.add(order);	
			}
			
			return orders;
		}
		catch (Exception e) {
			System.out.println("Error on getOrders");
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean addOrder(Order order) throws SQLException{
		
		boolean checkAdd = false;
		
		try {
			statement = (PreparedStatement) connection.prepareStatement("insert into WarehouseDB.comanda " +"(idClient,idProdus,cantitate,pret,data)" + " values(?,?,?,?,?)");
			statement.setInt(1, order.getCustomerID());
			statement.setInt(2, order.getProductID());
			statement.setInt(3, order.getQuantity());
			statement.setDouble(4, order.getOrderPrice());
			long currentTime = Calendar.getInstance().getTimeInMillis();
			Date data = new Date(currentTime);
			statement.setDate(5, data);
			statement.executeUpdate();
			checkAdd = true;
			
		}
		catch(Exception e) {
			System.out.println("Error on addOrder");
			e.printStackTrace();
			checkAdd = false;
		}
		
		return checkAdd;
	}
	
	public boolean deleteOrder(int orderID) throws SQLException{
		
		boolean checkDelete = false;
		
		try {
			
			statement = (PreparedStatement) connection.prepareStatement("delete from comanda where idComanda=?" );
			statement.setInt(1, orderID);
			statement.executeUpdate();
			checkDelete = true;
		}
		
		catch (Exception e) {
			System.out.println("Error on deleteOrder");
			e.printStackTrace();
			checkDelete = false;
		}
		
		return checkDelete;
	}
	
	public ArrayList<Order> searchOrder(String orderID) throws SQLException {

		try {
			orderID += "%";
			statement = (PreparedStatement) connection.prepareStatement("select * from comanda where idComanda like ?");
			statement.setString(1, orderID);
			rs = statement.executeQuery();

			while (rs.next()) {
				order = new Order();
				order.setOrderID(rs.getInt(1));
				order.setCustomerID(rs.getInt(2));
				order.setProductID(rs.getInt(3));
				order.setQuantity(rs.getInt(4));
				order.setOrderPrice(rs.getDouble(5));
				order.setOrderDate(rs.getDate(6));
				orders.add(order);

			}
			return orders;
		} catch (Exception e) {
			System.out.println("Error on customerSearch!");
			e.printStackTrace();
			return null;
		}
	}
	
	public Order searchOrderF(String orderID) throws SQLException {

		try {
			orderID += "%";
			statement = (PreparedStatement) connection.prepareStatement("select * from comanda where idComanda like ?");
			statement.setString(1, orderID);
			rs = statement.executeQuery();

			while (rs.next()) {
				order = new Order();
				order.setOrderID(rs.getInt(1));
				order.setCustomerID(rs.getInt(2));
				order.setProductID(rs.getInt(3));
				order.setQuantity(rs.getInt(4));
				order.setOrderPrice(rs.getDouble(5));
				order.setOrderDate(rs.getDate(6));
				

			}
			return order;
		} catch (Exception e) {
			System.out.println("Error on customerSearch!");
			e.printStackTrace();
			return null;
		}
	}
	
}
