package dataAccessLayer;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import model.Customer;

public class CustomerDAO {

	private ArrayList<Customer> customers = new ArrayList<Customer>();
	private PreparedStatement statement = null;
	private ResultSet rs = null;
	private Statement st = null;
	private Connection connection;
	private Customer customer;

	public CustomerDAO() throws ClassNotFoundException, SQLException {

		connection = DataBaseConnection.connect();

	}

	public ArrayList<Customer> getCustomers() throws SQLException {
		try {
			st = (Statement) connection.createStatement();
			rs = st.executeQuery("Select * from client");

			while (rs.next()) {

				customer = new Customer();
				customer.setCustomerID(rs.getInt(1));
				customer.setCustomerName(rs.getString(2));
				customer.setCustomerPhone(rs.getString(3));
				customer.setCustomerAddress(rs.getString(4));
				customer.setCustomerStateAndCountry(rs.getString(5));
				customer.setCustomerMail(rs.getString(6));
				customers.add(customer);
			}
		
			return customers;

		}

		catch (SQLException e) {
			System.out.println("Error on retrieving customer CustomerDAO");
			e.printStackTrace();
			return null;
		}

	}


	public ArrayList<Customer> searchCustomer(String customerName) throws SQLException {

		try {
			customerName += "%";
			statement = (PreparedStatement) connection.prepareStatement("select * from client where nume like ?");
			statement.setString(1, customerName);
			rs = statement.executeQuery();

			while (rs.next()) {
				customer = new Customer();
				customer.setCustomerID(rs.getInt(1));
				customer.setCustomerName(rs.getString(2));
				customer.setCustomerPhone(rs.getString(3));
				customer.setCustomerAddress(rs.getString(4));
				customer.setCustomerStateAndCountry(rs.getString(5));
				customer.setCustomerMail(rs.getString(6));
				customers.add(customer);

			}
			return customers;
		} catch (Exception e) {
			System.out.println("Error on customerSearch!");
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean deleteCustomer(int customerID) throws SQLException {

		boolean checkDelete = false;

		try {

			statement = (PreparedStatement) connection.prepareStatement("delete from Client where idClient=?");
			checkDelete = true;
			statement.setInt(1, customerID);
			statement.executeUpdate();

		} catch (Exception e) {
			checkDelete = false;
			System.out.println("Error on delete customer!");
			e.printStackTrace();
		}

		return checkDelete;
	}

	public boolean addCustomer(Customer customer) throws SQLException{
		
		boolean checkAdd = false;
		
		try {
			
			statement = (PreparedStatement) connection.prepareStatement("insert into client" + "(nume,adresa,telefon,mail,tarajudet)" + "values (?,?,?,?,?)");
			statement.setString(1, customer.getCustomerName());
			statement.setString(2, customer.getCustomerAddress());
			statement.setString(3, customer.getCustomerPhone());
			statement.setString(4, customer.getCustomerMail());	
			statement.setString(5, customer.getCustomerStateAndCountry());
			statement.executeUpdate();
			checkAdd = true;
		}
		catch (Exception e) {
			System.out.println("Error on addCustomer");
			e.printStackTrace();
			checkAdd = false;
		}
		
		return checkAdd;
	}
	
	public boolean changeCustomer(Customer customer) throws SQLException{
		
		boolean checkChange = false;
		
		try {
			
			statement = (PreparedStatement) connection.prepareStatement("update client" + " set nume=?,adresa=?,telefon=?,mail=?,tarajudet=?" + " where idClient=?");
		
			statement.setString(1, customer.getCustomerName());
			statement.setString(2, customer.getCustomerAddress());
			statement.setString(3, customer.getCustomerPhone());
			statement.setString(4, customer.getCustomerMail());	
			statement.setString(5, customer.getCustomerStateAndCountry());
			statement.setInt(6, customer.getCustomerID());
			statement.executeUpdate();
			checkChange = true;
		}
		
		catch(Exception e) {
			System.out.println("Error on changeCustomer!");
			e.printStackTrace();
			checkChange = false;
		}
		
		return checkChange;
	}

}
